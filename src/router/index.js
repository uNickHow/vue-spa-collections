import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import LinkStyle from '@/components/LinkStyle'

Vue.use(Router)

export default new Router({
  mode: 'history',

  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/link-style',
      name: 'Link Style',
      component: LinkStyle
    }
  ]
})
